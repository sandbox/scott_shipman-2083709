<?php
/**
 * @file
 * This is the module file for Content Access VBO.
 *
 * This file defines all the functions used for this module.
 */

/**
 * Implements hook_perm().
 *
function content_access_vbo_permission() {
  return array(
    'grant bulk content access' => array(
      'title' => t('Manage bulk content access'),
      'description' => t('View and modify content access for multiple  nodes'),
    ),
  );
}
 * /

/**
 * Implements hook_action_info
 *
 * see info at 
 * https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_action_info/7
 *
 */

function content_access_vbo_action_info() {
  return array(
    'content_access_vbo_action' => array(
      'type' => 'node',
      'label' => t('Assign node access to roles'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => TRUE,
      'triggers' => array('any'),
      'pass rows' => TRUE,
    ),
  );
}


/**
 * Form constructor for the VBO configuration options and per-bulk setting.
 *
 * Defines the VBO configuration options and per-bulk setting
 * @param string $settings
 *   The settings for the form
 *
 * see @content_access_vbo_action_submit
 */

function content_access_vbo_action_form($settings) {
  $operations = _content_access_get_operations();
  $type=isset($_GET['type']) ? $_GET['type'] : NULL ;

  // Get roles form
    $form['content_access_vbo_roles'] = array(
        '#type' => 'fieldset',
        '#title' => t('Role based access control settings'),
        '#collapsible' => TRUE,
        '#description' => t('<p><strong>IMPORTANT: You may be overwriting existing Content access permissions which are not reflected as defaults on this form. Use with caution </strong><p>Note that users need at least the %access_content permission to be able to deal in any way with content.', array('%access_content' => t('access content'))) .
          ' ' . t('Furthermore note that content which is not @published is treated in a different way by drupal: It can be viewed only by its author or users with the %administer_nodes permission.', array('@published' => t('published'), '%administer_nodes' => t('administer nodes'))),
    );


    $defaults = content_access_get_setting_defaults($type);

    $roles = array_map('filter_xss_admin', user_roles());
    foreach ($operations as $op => $label) {


        $form['content_access_vbo_roles'][$op] = array('#type' => 'checkboxes',
          '#prefix' => '<div class="content_access-div">',
          '#suffix' => '</div>',
          '#options' => $roles,
          '#title' => t($label),
          '#default_value' => $defaults[$op],
          '#process' => array('form_process_checkboxes', 'content_access_disable_checkboxes'),
         );
    }

    $form['content_access_vbo_roles']['clearer'] = array(
    '#value' => '<br clear="all" />',
     );



  // Add an after_build handler that disables checkboxes, which are enforced by permissions.
  //  $form['content_access_vbo_roles']['#after_build'] = array('content_access_force_permissions');
  drupal_add_css(drupal_get_path('module', 'content_access') . '/content_access.css');
  return $form;
}


/**
 * Form submission for the VBO configuration options and per-bulk setting.
 *
 * see @content_access_vbo_action_form
 */

function content_access_vbo_action_submit($form, $form_state) {
 
  $return = array('set_roles' => array('view'=> $form_state['values']['view'],
                                       'view_own'=> $form_state['values']['view_own'],
                                       'update'=> $form_state['values']['update'],
                                       'update_own'=> $form_state['values']['update_own'],
                                       'delete'=> $form_state['values']['delete'],
                                       'delete_own'=> $form_state['values']['delete_own'],), );
  return $return;
}


/**
 * Define the action which sets the access grants
 *
 */

function content_access_vbo_action(&$node, $context) {
// add logic to update grants
$settings=array();
foreach($context['set_roles'] as $op=>$string) {
   //for each operation, get RIDs that have 1 or TRUE values
  foreach ($string as $rid=>$value) {
	//build values to update in db
       if($value==1 || $value== TRUE){ $settings[$op][] = $rid;}
  }
 }
 //update the database's content_access table with serealized data

 // if there is already a node->nid record, do update, else do insert
$count = db_select('content_access')
            ->condition('nid', $node->nid)
            ->countQuery()->execute()->fetchField();

  if ($count > 0) {
    db_update('content_access')
      ->condition('nid', $node->nid)
      ->fields(array('settings' => serialize($settings)))
      ->execute();
  }
  else {
    db_insert('content_access')
      ->fields(array('nid' => $node->nid, 'settings' => serialize($settings)))
      ->execute();
  }


}

/**
 * Implement hook_views_api
 *
 */

function content_access_vbo_views_api() {
   return array(
        'api' => 3.0,
        );
}

/**
 * Implements hook_views_default_views()
 *
 */

function content_access_vbo_views_default_views() {
// export of view here
$view = new view();
$view->name = 'update_permission_in_bulk';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Update Permission in Bulk';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Content Permission Bulk Update';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  4 => '4',
  24 => '24',
  23 => '23',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'input_required';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
$handler->display->display_options['exposed_form']['options']['text_input_required'] = 'Select any filter and click on Filterto see results';
$handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'full_html';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'views_bulk_operations' => 'views_bulk_operations',
  'type' => 'type',
  'title' => 'title',
  'name' => 'name',
  'view_node' => 'view_node',
  'status' => 'status',
  'created' => 'created',
  'changed' => 'changed',
);
$handler->display->display_options['style_options']['default'] = 'created';
$handler->display->display_options['style_options']['info'] = array(
  'views_bulk_operations' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'type' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'view_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Instructions:';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<strong>Instructions:</strong>
1. Select the filter criteria to return a list of content pages (nodes)
2. Select specific pages to edit access rules by checking the respective checkboxes
3. Click the \'Assign node access to roles\' button.';
$handler->display->display_options['header']['area']['format'] = 'full_html';
/* Relationship: Content: Author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Bulk operations: Content */
$handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
$handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['label'] = 'Select';
$handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
  'action::content_access_vbo_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_assign_owner_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::views_bulk_operations_delete_item' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::views_bulk_operations_script_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::flag_node_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_make_sticky_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_make_unsticky_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::views_bulk_operations_modify_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
    'settings' => array(
      'show_all_tokens' => 0,
      'display_values' => array(
        '_all_' => '_all_',
      ),
    ),
  ),
  'action::views_bulk_operations_argument_selector_action' => array(
    'selected' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
    'settings' => array(
      'url' => '',
    ),
  ),
  'action::node_promote_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_publish_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_unpromote_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_save_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::system_send_email_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::workbench_moderation_set_state_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_unpublish_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_unpublish_by_keyword_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::pathauto_node_update_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
);
/* Field: Content: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'node';
$handler->display->display_options['fields']['type']['field'] = 'type';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Author';
/* Field: Content: Link */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
/* Field: Content: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'long';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'long';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'alert_promo' => 'alert_promo',
  'article' => 'article',
  'page' => 'page',
  'classified' => 'classified',
  'heiskell_award' => 'heiskell_award',
  'contact_info' => 'contact_info',
  'contest' => 'contest',
  'deal' => 'deal',
  'division_logo' => 'division_logo',
  'event' => 'event',
  'executive_message' => 'executive_message',
  'faq' => 'faq',
  'featured_promo' => 'featured_promo',
  'gallery' => 'gallery',
  'image' => 'image',
  'landing_page' => 'landing_page',
  'myslideshow' => 'myslideshow',
  'promotion_hire' => 'promotion_hire',
  'news_story' => 'news_story',
  'newsletter' => 'newsletter',
  'potm' => 'potm',
  'sticky_quick_link' => 'sticky_quick_link',
  'permissioned_landing_page' => 'permissioned_landing_page',
  'poll' => 'poll',
  'press_feed' => 'press_feed',
  'promo' => 'promo',
  'quick_link' => 'quick_link',
  'site_page' => 'site_page',
  'social_media_link' => 'social_media_link',
  'ttime_entry' => 'ttime_entry',
  'webform' => 'webform',
);
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  29 => 0,
  30 => 0,
  4 => 0,
  3 => 0,
  17 => 0,
  18 => 0,
  23 => 0,
  24 => 0,
  31 => 0,
  32 => 0,
  33 => 0,
  34 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
  16 => 0,
  19 => 0,
  20 => 0,
  25 => 0,
  26 => 0,
  38 => 0,
  35 => 0,
  39 => 0,
  36 => 0,
  40 => 0,
  37 => 0,
  41 => 0,
  42 => 0,
  43 => 0,
  44 => 0,
  45 => 0,
  46 => 0,
  47 => 0,
  48 => 0,
  49 => 0,
  50 => 0,
  51 => 0,
  52 => 0,
  53 => 0,
  54 => 0,
  55 => 0,
  56 => 0,
  57 => 0,
  58 => 0,
);
$handler->display->display_options['filters']['type']['group_info']['label'] = 'Type';
$handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['group_info']['remember'] = FALSE;
$handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
  1 => array(),
  2 => array(),
  3 => array(),
);
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status_1']['id'] = 'status_1';
$handler->display->display_options['filters']['status_1']['table'] = 'node';
$handler->display->display_options['filters']['status_1']['field'] = 'status';
$handler->display->display_options['filters']['status_1']['value'] = 'All';
$handler->display->display_options['filters']['status_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['status_1']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status_1']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status_1']['expose']['operator'] = 'status_1_op';
$handler->display->display_options['filters']['status_1']['expose']['identifier'] = 'status_1';
$handler->display->display_options['filters']['status_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  29 => 0,
  30 => 0,
  4 => 0,
  3 => 0,
  17 => 0,
  18 => 0,
  23 => 0,
  24 => 0,
  31 => 0,
  32 => 0,
  33 => 0,
  34 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
  16 => 0,
  19 => 0,
  20 => 0,
  25 => 0,
  26 => 0,
  38 => 0,
  35 => 0,
  39 => 0,
  36 => 0,
  40 => 0,
  37 => 0,
  41 => 0,
  42 => 0,
  43 => 0,
  44 => 0,
  45 => 0,
  46 => 0,
  47 => 0,
  48 => 0,
  49 => 0,
  50 => 0,
  51 => 0,
  52 => 0,
  53 => 0,
  54 => 0,
  55 => 0,
  56 => 0,
  57 => 0,
  58 => 0,
);
/* Filter criterion: User: Name */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'users';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['relationship'] = 'uid';
$handler->display->display_options['filters']['uid']['value'] = '';
$handler->display->display_options['filters']['uid']['exposed'] = TRUE;
$handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['label'] = 'Author';
$handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
$handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  29 => 0,
  30 => 0,
  4 => 0,
  3 => 0,
  17 => 0,
  18 => 0,
  23 => 0,
  24 => 0,
  31 => 0,
  32 => 0,
  33 => 0,
  34 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
  16 => 0,
  19 => 0,
  20 => 0,
  25 => 0,
  26 => 0,
  38 => 0,
  35 => 0,
  39 => 0,
  36 => 0,
  40 => 0,
  37 => 0,
  41 => 0,
  42 => 0,
  43 => 0,
  44 => 0,
  45 => 0,
  46 => 0,
  47 => 0,
  48 => 0,
  49 => 0,
  50 => 0,
  51 => 0,
  52 => 0,
  53 => 0,
  54 => 0,
  55 => 0,
  56 => 0,
  57 => 0,
  58 => 0,
);
/* Filter criterion: Content: Updated date */
$handler->display->display_options['filters']['changed']['id'] = 'changed';
$handler->display->display_options['filters']['changed']['table'] = 'node';
$handler->display->display_options['filters']['changed']['field'] = 'changed';
$handler->display->display_options['filters']['changed']['operator'] = 'between';
$handler->display->display_options['filters']['changed']['exposed'] = TRUE;
$handler->display->display_options['filters']['changed']['expose']['operator_id'] = 'changed_op';
$handler->display->display_options['filters']['changed']['expose']['label'] = 'Last Updated Date Between';
$handler->display->display_options['filters']['changed']['expose']['description'] = 'enter date formats as MM/DD/YY, ex: 8/17/13';
$handler->display->display_options['filters']['changed']['expose']['operator'] = 'changed_op';
$handler->display->display_options['filters']['changed']['expose']['identifier'] = 'changed';
$handler->display->display_options['filters']['changed']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  29 => 0,
  30 => 0,
  4 => 0,
  3 => 0,
  17 => 0,
  18 => 0,
  23 => 0,
  24 => 0,
  31 => 0,
  32 => 0,
  33 => 0,
  34 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
  16 => 0,
  19 => 0,
  20 => 0,
  25 => 0,
  26 => 0,
  38 => 0,
  35 => 0,
  39 => 0,
  36 => 0,
  40 => 0,
  37 => 0,
  41 => 0,
  42 => 0,
  43 => 0,
  44 => 0,
  45 => 0,
  46 => 0,
  47 => 0,
  48 => 0,
  49 => 0,
  50 => 0,
  51 => 0,
  52 => 0,
  53 => 0,
  54 => 0,
  55 => 0,
  56 => 0,
  57 => 0,
  58 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'admin/config/people/content-access-vbo';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Update Permission in Bulk';
$handler->display->display_options['menu']['description'] = 'Update Permissions in Bulk';
$handler->display->display_options['menu']['weight'] = '-100';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;

$views[$view->name] = $view;
return $views;
}
