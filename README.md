CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Content Access VBO module creates an action for Views Bulk Operations 
to update per node content access (view/edit/delete etc). It adds a menu item 
to the Content menu tree and adds a new view which lists content with the VBO action as a button..

* For a full description of the module, visit the project page:
https://www.drupal.org/sandbox/scott_shipman/2083709

    

REQUIREMENTS
------------

This module requires the following modules:

 * Views Bulk Operations (https://drupal.org/project/views_bulk_operations)
 * Content Access (https://drupal.org/project/content_access)    
 
 
RECOMMENDED MODULES
-------------------

 * N/A
 
 
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 
 
CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:
        Assign 'Assign node access to roles' to the roles you wish to give this privelege
        
 * Change node access permissions at
        Administration->People->Update Permission in Bulk


MAINTAINERS
-----------

Current maintainers:
 * Scott Shipman (scott shipman) - https://www.drupal.org/u/scott-shipman